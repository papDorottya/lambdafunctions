import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    private List<MonitoredData> monitoredData;

    private String inputFile = "Activity.txt";

    private Path inPath = Paths.get(inputFile);

    public static void main(String[] args) {
        App m = new App();
        m.generateList();
        m.nbOfMonitoredDays();
        m.nbOfEachActivity();
        m.nbOfEachActivityPerDay();
        m.totalTimePerActivities();
        m.filteredActivity();
        m.writeResult();
    }

    private List<MonitoredData> generateList() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try (Stream<String> stringStream = Files.lines(inPath)) {
            monitoredData = stringStream.map(d -> {
                try {
                    String[] row = d.split("\t\t");
                    return new MonitoredData(dateFormat.parse(row[0]), dateFormat.parse(row[1]), row[2].trim());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                return null;
            })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private int nbOfMonitoredDays() {

        Stream<String> startDates =  monitoredData.stream()
                .map(m -> m.getDateWithoutTimeStart())
                .distinct();
        Stream<String> endDates =  monitoredData.stream()
                .map(m -> m.getDateWithoutTimeEnd())
                .distinct();

        return Math.toIntExact(Stream.concat(startDates, endDates).distinct().count());
    }

    private Map<String, Integer> nbOfEachActivity() {
        return monitoredData.stream()
                .collect(Collectors.groupingBy(
                        MonitoredData::getActivity,
                        Collectors.collectingAndThen(
                                Collectors.mapping(MonitoredData::getActivity,
                                        Collectors.counting()),
                                Long::intValue
                        )));
    }

    private Map<Integer, Map<String, Integer>> nbOfEachActivityPerDay() {
        return monitoredData.stream()
                .collect(
                        Collectors.groupingBy(
                                MonitoredData::getDay,
                                Collectors.groupingBy(
                                        MonitoredData::getActivity,
                                        Collectors.collectingAndThen(
                                                Collectors.mapping(MonitoredData::getActivity, Collectors.counting()), Long::intValue)
                                )
                        )
                );
    }

    private Map<String, Integer> totalTimePerActivities() {
        return monitoredData.stream()
                .collect(
                        Collectors.groupingBy(
                                MonitoredData::getActivity,
                                Collectors.summingInt(
                                        MonitoredData::getDurationMinute
                                )
                        )
                );
    }

    private List<String> filteredActivity() {
        return monitoredData.stream()
                .filter(md -> getFilteredTime(md) != null)
                .map(m -> m.getActivity())
                .distinct()
                .collect(Collectors.toList());
    }

    private String getFilteredTime(MonitoredData md) {
        List<MonitoredData> activities = monitoredData
                .stream()
                .filter(m -> m.getActivity().equals(md.getActivity()))
                .collect(Collectors.toList());

        int counter = (int) activities
                .stream()
                .filter(m -> m.getDuration() / 1000 / 60 < 5.0)
                .count();

        if (counter > (double) activities.size() * 0.9)
            return md.getActivity();

        return null;
    }

    private void writeResult() {
        String outputFile1 = "Task_";
        File outFile = new File(outputFile1.concat("1.txt"));
        try {
            FileWriter writer = new FileWriter(outFile);

            writer.write("The list of activities\n");

            for (MonitoredData md : monitoredData) {
                writer.write(md + "\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        outFile = new File(outputFile1.concat("2.txt"));
        try {
            FileWriter writer = new FileWriter(outFile);

            writer.write("Number of monitored days is ");
            writer.write(String.valueOf(nbOfMonitoredDays()));

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        outFile = new File(outputFile1.concat("3.txt"));
        try {
            FileWriter writer = new FileWriter(outFile);

            writer.write("Number of each activity:");
            for (String activity : nbOfEachActivity().keySet())
                writer.write("\n" + activity + ": " + nbOfEachActivity().get(activity));

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        outFile = new File(outputFile1.concat("4.txt"));
        try {
            FileWriter writer = new FileWriter(outFile);

            writer.write("Number of each activity per day:");
            for (Integer integer : nbOfEachActivityPerDay().keySet()) {
                writer.write("\n" + integer + "\n");
                for (String a : nbOfEachActivityPerDay().get(integer).keySet())
                    writer.write(a + ": " + nbOfEachActivityPerDay().get(integer).get(a) + "\n");
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        outFile = new File(outputFile1.concat("5.txt"));
        try {
            FileWriter writer = new FileWriter(outFile);
            writer.write("Total time per activities");
            for (String s : totalTimePerActivities().keySet()) {
                writer.write("\n" + s + " " + totalTimePerActivities().get(s));
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        outFile = new File(outputFile1.concat("6.txt"));
        try {
            FileWriter writer = new FileWriter(outFile);

            writer.write("Filtered activities that have more than 90% of the monitoring records with duration less than 5 min is ");
            for (String s : filteredActivity()) {
                writer.write(s + "\n");
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
