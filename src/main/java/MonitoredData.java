import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class MonitoredData {
    private Date startTime;
    private Date endTime;
    private String activity;


    public MonitoredData(Date startTime, Date endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getDateWithoutTimeStart() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getStartTime());
        return cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH)+ "-" + cal.get(Calendar.DAY_OF_MONTH);
    }

    public String getDateWithoutTimeEnd() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getEndTime());
        return cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH)+ "-" + cal.get(Calendar.DAY_OF_MONTH);
    }

    public int getDay() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getStartTime());
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public long getDuration() {
        return endTime.getTime() - startTime.getTime();
    }

    public Integer getDurationMinute() {
        System.out.println((int)TimeUnit.MINUTES.convert(getDuration(), TimeUnit.MILLISECONDS));
        return (int)TimeUnit.MINUTES.convert(getDuration(), TimeUnit.MILLISECONDS);
    }
    
    @Override
    public String toString() {
        return  startTime + "   " + endTime + "   " + activity;
    }
}
